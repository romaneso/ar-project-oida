﻿
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class InstantiateGameManagerScript : MonoBehaviour
{
    public Button AddYellowPlayerButton;
    public Button AddBluePlayerButton;
    public Button AddGreenPlayerButton;
    public Button AddRedPlayerButton;
    public Button StartGameButton;

    public GameObject YellowBase;
    public GameObject BlueBase;
    public GameObject GreenBase;
    public GameObject RedBase;

    public GameObject Fields;

    public GameObject[] YellowFigures;
    public GameObject[] BlueFigures;
    public GameObject[] GreenFigures;
    public GameObject[] RedFigures;

    public bool YellowOptIn = false;
    public bool BlueOptIn = false;
    public bool GreenOptIn = false;
    public bool RedOptIn = false;

    public GameObject YellowFinish;
    public GameObject BlueFinish;
    public GameObject GreenFinish;
    public GameObject RedFinish;


    void Start()
    {
        Fields.SetActive(false);

        YellowBase.SetActive(false);
        BlueBase.SetActive(false);
        GreenBase.SetActive(false);
        RedBase.SetActive(false);


        YellowFigures.ToList().ForEach(figure => {
            var fieldRenderer = figure.GetComponent<Renderer>();
            var specifiedColor = Color.yellow;
            fieldRenderer.material.SetColor("_Color", specifiedColor);
        });

        GreenFigures.ToList().ForEach(figure => {
            var fieldRenderer = figure.GetComponent<Renderer>();
            var specifiedColor = Color.green;
            fieldRenderer.material.SetColor("_Color", specifiedColor);
        });

        BlueFigures.ToList().ForEach(figure => {
            var fieldRenderer = figure.GetComponent<Renderer>();
            var specifiedColor = Color.blue;
            fieldRenderer.material.SetColor("_Color", specifiedColor);
        });

        RedFigures.ToList().ForEach(figure => {
            var fieldRenderer = figure.GetComponent<Renderer>();
            var specifiedColor = Color.red;
            fieldRenderer.material.SetColor("_Color", specifiedColor);
        });


        Array.ForEach(YellowFigures, figure =>
        {
            figure.SetActive(false);
        });
        Array.ForEach(BlueFigures, figure =>
        {
            figure.SetActive(false);
        });
        Array.ForEach(GreenFigures, figure =>
        {
            figure.SetActive(false);
        });
        Array.ForEach(RedFigures, figure =>
        {
            figure.SetActive(false);
        });

        AddYellowPlayerButton.onClick.AddListener(YellowPlayerBtnClicked);
        AddBluePlayerButton.onClick.AddListener(AddBluePlayerButtonClicked);
        AddGreenPlayerButton.onClick.AddListener(AddGreenPlayerButtonClicked);
        AddRedPlayerButton.onClick.AddListener(AddRedPlayerButtonClicked);
        StartGameButton.onClick.AddListener(StartGameButtonClicked);
    }

    private void YellowPlayerBtnClicked()
    {
        YellowBase.SetActive(true);
        AddYellowPlayerButton.gameObject.SetActive(false);

        Array.ForEach(YellowFigures, figure =>
        {
            figure.SetActive(true);
        });

        YellowOptIn = true;
    }

    private void AddBluePlayerButtonClicked()
    {
        BlueBase.SetActive(true);
        AddBluePlayerButton.gameObject.SetActive(false);

        Array.ForEach(BlueFigures, figure =>
        {
            figure.SetActive(true);
        });
      
        BlueOptIn = true;
    }

    private void AddGreenPlayerButtonClicked()
    {
        GreenBase.SetActive(true);
        AddGreenPlayerButton.gameObject.SetActive(false);

        Array.ForEach(GreenFigures, figure =>
        {
            figure.SetActive(true);
        });

        GreenOptIn = true;
    }

    private void AddRedPlayerButtonClicked()
    {
        RedBase.SetActive(true);
        AddRedPlayerButton.gameObject.SetActive(false);

        Array.ForEach(RedFigures, figure =>
        {
            figure.SetActive(true);
        });

        RedOptIn = true;
    }

    private void StartGameButtonClicked()
    {
        //don't activate game if noone opt in
        if (!RedOptIn && !BlueOptIn && !GreenOptIn && !YellowOptIn) return;

        Fields.SetActive(true);
        StartGameButton.gameObject.SetActive(false);

        var gameObject = new GameObject("GameManager");
        gameObject.AddComponent<GameManagerScript>();

        //disable all play buttons
        AddBluePlayerButton.gameObject.SetActive(false);
        AddYellowPlayerButton.gameObject.SetActive(false);
        AddRedPlayerButton.gameObject.SetActive(false);
        AddGreenPlayerButton.gameObject.SetActive(false);

        var component = gameObject.GetComponent<GameManagerScript>();
        if (YellowOptIn)
        {
            var player = new Player() {
                PlayerBase = YellowBase,
                PlayerStartField = GameObject.Find("Fields").transform.Find("Board").transform.Find("YellowStart").gameObject,
                PlayerFinishField = YellowFinish,
                Color = MyColor.YELLOW
            };

            Array.ForEach(YellowFigures, figure =>
            {
                player.Figures.Add(new Figure() {
                    GameObject = figure,
                    InitialPosition = new Vector3(figure.transform.position.x, figure.transform.position.y, figure.transform.position.z),
                    IsInField = false
                });
            });

            component.Players.Add(player);
        }


        if (BlueOptIn)
        {
            var player = new Player()
            {
                PlayerBase = BlueBase,
                PlayerStartField = GameObject.Find("Fields").transform.Find("Board").transform.Find("BlueStart").gameObject,
                PlayerFinishField = BlueFinish,
                Color = MyColor.BLUE
            };

            Array.ForEach(BlueFigures, figure =>
            {
                player.Figures.Add(new Figure()
                {
                    GameObject = figure,
                    InitialPosition = new Vector3(figure.transform.position.x, figure.transform.position.y, figure.transform.position.z),
                    IsInField = false
                });
            });

            component.Players.Add(player);
        }

        if (GreenOptIn)
        {
            var player = new Player()
            {
                PlayerBase = GreenBase,
                PlayerStartField = GameObject.Find("Fields").transform.Find("Board").transform.Find("GreenStart").gameObject,
                PlayerFinishField = GreenFinish,
                Color = MyColor.GREEN
            };

            Array.ForEach(GreenFigures, figure =>
            {
                player.Figures.Add(new Figure()
                {
                    GameObject = figure,
                    InitialPosition = new Vector3(figure.transform.position.x, figure.transform.position.y, figure.transform.position.z),
                    IsInField = false
                });
            });

            component.Players.Add(player);
        }

        if (RedOptIn)
        {
            var player = new Player()
            {
                PlayerBase = RedBase,
                PlayerStartField = GameObject.Find("Fields").transform.Find("Board").transform.Find("RedStart").gameObject,
                PlayerFinishField = RedFinish,
                Color = MyColor.RED
            };

            Array.ForEach(RedFigures, figure =>
            {
                player.Figures.Add(new Figure()
                {
                    GameObject = figure,
                    InitialPosition = new Vector3(figure.transform.position.x, figure.transform.position.y, figure.transform.position.z),
                    IsInField = false
                });
            });

            component.Players.Add(player);
        }

        Destroy(this);

        //start game of gamemanagerscript
        component.StartGame();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
