﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
    public Boolean ShowAnimations = true;

    //active players are here (if player didn't opt in, not in here!)
    public List<Player> Players = new List<Player>();

    //Player that should roll the dice
    public Player CurrentPlayer = null;

    //all field references 
    private GameObject fieldReference;
    private GameObject boardFields;

    //this button should fire the roll dice function
    private Button RollDiceButton;

    private GameObject canvas;
    private GameObject imageTarget;

    //update current player with labels
    private GameObject uiOverlayCanvas;
    private GameObject statusBar;
    private GameObject statusUpdateLabel;

    //used to delete dice that is being thrown
    private GameObject myInitiatedDice;

    //emitting particles in color of player
    private GameObject particleSystem;
    private ParticleColorScript particleSystemScript;

    // Start is called before the first frame update
    void Start()
    {
        InitButtons();
        PrepareForNextThrow();
    }

    //used for showing field animations
    private bool beingHandled = false;
    void Update()
    {
        if (ShowAnimations)
        {
            if (!beingHandled)
            {
                StartCoroutine(AnimateFields());
            }
        }
    }


  
    private IEnumerator AnimateFields()
    {
        beingHandled = true;
        // process pre-yield

        yield return FieldAnimations.RandomAnimation(boardFields);

        // process post-yield
        beingHandled = false;
    }

    private void PrepareForNextThrow()
    {

        for (int i = 0; i < Players.Count; i++)
        {
            if(CurrentPlayer == Players[i])
            {
                try
                {
                    CurrentPlayer = Players[i + 1];
                }catch(System.ArgumentOutOfRangeException ex)
                {
                    CurrentPlayer = Players[0];
                }

                break;
            }
        }


        var text = "";
        switch (CurrentPlayer.Color)
        {
            case MyColor.BLUE:
                text = "BLUEs turn!";

                particleSystem.transform.position = CurrentPlayer.PlayerBase.transform.position;
                particleSystemScript.ChangeToBlueGradient();

                particleSystem.SetActive(true);
                break;
            case MyColor.GREEN:
                text = "GREENs turn!";

                particleSystem.transform.position = CurrentPlayer.PlayerBase.transform.position;
                particleSystemScript.ChangeToGreenGradient();

                particleSystem.SetActive(true);
                break;
            case MyColor.RED:
                text = "REDs turn!";

                particleSystem.transform.position = CurrentPlayer.PlayerBase.transform.position;
                particleSystemScript.ChangeToRedGradient();

                particleSystem.SetActive(true);
                break;
            case MyColor.YELLOW:
                text = "YELLOWs turn!";

                particleSystem.transform.position = CurrentPlayer.PlayerBase.transform.position;
                particleSystemScript.ChangeToYellowGradient();

                particleSystem.SetActive(true);
                break;
        }
        try
        {
            SetTextUpdate(text);
        }catch (Exception)
        {
            
        }
        
    }

    public void StartGame()
    {
        CurrentPlayer = Players.First();

    }

    private void SetTextUpdate(String textUpdate)
    {
        var textComponent = statusUpdateLabel.GetComponent<Text>();
        textComponent.text = textUpdate;
    }

    private void RollDiceBtnClicked()
    {
        //fire dice
        var Dice = Resources.Load("NewFinalDice", typeof(GameObject));

        if (myInitiatedDice != null) Destroy(myInitiatedDice);
        myInitiatedDice = Instantiate(Dice, imageTarget.transform) as GameObject;
        var diceScript = myInitiatedDice.GetComponent<NewDiceScript>();
        DiceHitGroundCollider.listener = (number) => {
            MakeMoveWithDiceNumber(number);
            Destroy(myInitiatedDice);
        };
    }

    private void MakeMoveWithDiceNumber(int diceNumber)
    {
        //find out if we have to move new figure to play or move any
        Boolean hasNotActiveFigures = false;
        if (diceNumber == 6)
        {
            //find out it player has any inactive figures, if so, move it to field
            foreach (var figure in CurrentPlayer.Figures)
            {
                if (!figure.IsInField && !figure.IsInFinish)
                {
                    figure.GameObject.transform.position = new Vector3(CurrentPlayer.PlayerStartField.transform.position.x, figure.GameObject.transform.position.y, CurrentPlayer.PlayerStartField.transform.position.z);
                    figure.currentField = CurrentPlayer.PlayerStartField;

                    figure.IsInField = true;
                    hasNotActiveFigures = true;

                    ShowAnimations = false;
                    break;
                }
            }
        }

        //if all figures are active, move figure further
        if (!hasNotActiveFigures)
        {
            //find random figure to move
            System.Random random = new System.Random();
            //CurrentPlayer.Figures.Select(a => a.is).Where(a => { return true; });

            var activeFigures = CurrentPlayer.Figures.Where(figure => figure.IsInField && !figure.IsInFinish).ToList();
            if (activeFigures.Count > 0)
            {
                int randomFigureIndex = random.Next(0, activeFigures.Count);
                Figure randomFigure = activeFigures[randomFigureIndex];

                var myCountIndex = 0;
                var foundElement = false;
                for (var i = 0; ; i++)
                {
                    //go back to start if coming to end
                    if (i == boardFields.transform.childCount)
                    {
                        i = 0;
                    }

                    GameObject gameField = boardFields.transform.GetChild(i).gameObject;
                    if (gameField == randomFigure.currentField || foundElement)
                    {
                        foundElement = true;
                        myCountIndex++;


                        //check if next field is start of the player that is playing, if so, then move figure to finish
                        var possibleFinishField = GetFieldAtPosition(i + 1);
                        if (possibleFinishField == CurrentPlayer.PlayerStartField) // && myCountIndex+1 < diceNumber
                        {
                            MoveFigureToGameField(randomFigure, CurrentPlayer.PlayerFinishField);
                            randomFigure.IsInField = false;
                            randomFigure.IsInFinish = true;
                            Debug.Log("Player has one round with that field");

                            break;
                        }
                    }

                    if (myCountIndex - 1 == diceNumber)
                    {
                        //move figure to gameField, beforehand check if there is a figure at gameField position

                        var figureThatWouldBeOverdrawn = CheckIfFieldHasFigure(gameField);
                        if (figureThatWouldBeOverdrawn != null && !CheckIfFigureBelongsToCurrentPlayer(figureThatWouldBeOverdrawn))
                        {
                            MoveFigureToBaseField(figureThatWouldBeOverdrawn);
                        }

                        MoveFigureToGameField(randomFigure, gameField);


                        break;
                    }
                }
            }
        }

        PrepareForNextThrow();
    }

    private Boolean CheckIfFigureBelongsToCurrentPlayer(Figure figure)
    {
        foreach(var figureToCheck in CurrentPlayer.Figures)
        {
            if(figure == figureToCheck)
            {
                return true;
            }
        }

        return false;
    }

    private GameObject GetFieldAtPosition(int position)
    {
        //while(position >= boardFields.transform.childCount)
        //{
        //    position -= boardFields.transform.childCount;
        //}
        if (position == boardFields.transform.childCount)
        {
            position = 0;
        }

        return boardFields.transform.GetChild(position).gameObject;
    }


    private Figure CheckIfFieldHasFigure(GameObject gameField)
    {
        foreach(var player in Players)
        {
            var fields = player.Figures.Where(figure => figure.currentField == gameField);
            if(fields.Count() != 0)
            {
                return fields.ToList()[0];
            }
        }
        return null;
    }

    private void MoveFigureToGameField(Figure figure, GameObject gameField)
    {
        figure.GameObject.transform.position = new Vector3(gameField.transform.position.x, figure.GameObject.transform.position.y, gameField.transform.position.z);
        SetFigureField(figure, gameField);
    }

    private void MoveFigureToBaseField(Figure figure)
    {
        figure.GameObject.transform.position = new Vector3(figure.InitialPosition.x, figure.InitialPosition.y, figure.InitialPosition.z);
        figure.IsInField = false;
        SetFigureField(figure, null);
    }

    private void SetFigureField(Figure figure, GameObject gameObject)
    {
        figure.currentField = gameObject;
    }


    internal void InitButtons()
    {
        canvas = GameObject.Find("Canvas");
        uiOverlayCanvas = GameObject.Find("UIOverlayCanvas");
        imageTarget = GameObject.Find("ImageTarget");
        fieldReference = GameObject.Find("Fields");


        particleSystem = GetChildWithName(imageTarget, "ParticleSystem");
        particleSystemScript = particleSystem.GetComponent<ParticleColorScript>();

        boardFields = GetChildWithName(fieldReference, "Board");

        statusBar = uiOverlayCanvas.transform.Find("StatusBar").gameObject;
        statusBar.SetActive(true);
        statusUpdateLabel = statusBar.transform.Find("StatusUpdateLabel").gameObject;


        RollDiceButton = canvas.transform.Find("RollDiceBtn").GetComponent<Button>();
        RollDiceButton.onClick.AddListener(RollDiceBtnClicked);
        RollDiceButton.gameObject.SetActive(true);

        
    }

    GameObject GetChildWithName(GameObject obj, string name)
    {
        Transform trans = obj.transform;
        Transform childTrans = trans.Find(name);
        if (childTrans != null)
        {
            return childTrans.gameObject;
        }
        else
        {
            return null;
        }
    }

}

public class Player
{
    public GameObject PlayerBase;

    public List<Figure> Figures = new List<Figure>();

    public MyColor Color { get; internal set; }
    public GameObject PlayerStartField { get; internal set; }
    public GameObject PlayerFinishField { get; internal set; }
}

public enum MyColor
{
    RED, GREEN, BLUE, YELLOW
}

public class Figure
{
    public GameObject GameObject;
    public bool IsInField = false;
    internal GameObject currentField;

    public Vector3 InitialPosition { get; internal set; }
    public bool IsInFinish { get; internal set; }
}

public class FieldAnimations
{

    private static Color color = Color.red;


    private static DateTime startTime = DateTime.Now;
    private static int activeIndex = 0;
    public static IEnumerator RandomAnimation(GameObject objectToAnimate)
    {
        ArrayList animations = new ArrayList();
        animations.Add(Blink(objectToAnimate));
        animations.Add(GoThrough(objectToAnimate));

        if (startTime.AddMinutes(15) > DateTime.Now)
        {
            activeIndex++;
            startTime = DateTime.Now;
        }
        if (activeIndex == animations.Count)
        {
            activeIndex = 0;
        }

        yield return animations[0];
    }



    public static IEnumerator Blink(GameObject objectToAnimate)
    {
        if (color == Color.white)
        {
            color = Color.grey;
        }
        else
        {
            color = Color.white;
        }

        foreach (Transform child in objectToAnimate.transform)
        {
            //Get the Renderer component from the new cube
            var fieldRenderer = child.GetComponent<Renderer>();

            //Call SetColor using the shader property name "_Color" and setting the color to red
            fieldRenderer.material.SetColor("_Color", color);
        }


    
        yield return new WaitForSeconds(1.0f);
    }


    private static int activeGameObject = 0;
    public static IEnumerator GoThrough(GameObject objectToAnimate)
    {

        for(var i = 0; i < objectToAnimate.transform.childCount; i++)
        {
            Transform child = objectToAnimate.transform.GetChild(i);

            //Get the Renderer component from the new cube
            var fieldRenderer = child.GetComponent<Renderer>();
            var specifiedColor = Color.green;

            if (i == activeGameObject)
            {
                //Call SetColor using the shader property name "_Color" and setting the color to red
                specifiedColor = Color.yellow;
            }

            fieldRenderer.material.SetColor("_Color", specifiedColor);
        }

        //pointer to next object
        activeGameObject++;

        if(activeGameObject == objectToAnimate.transform.childCount)
        {
            activeGameObject = 0;
        }

        yield return new WaitForSeconds(0.1f);
    }

    public static IEnumerator GoNuts(GameObject objectToAnimate)
    {
        if (color == Color.blue)
        {
            color = Color.cyan;
        }
        else
        {
            color = Color.blue;
        }

        foreach (Transform child in objectToAnimate.transform)
        {
            //Get the Renderer component from the new cube
            var fieldRenderer = child.GetComponent<Renderer>();

            //Call SetColor using the shader property name "_Color" and setting the color to red
            fieldRenderer.material.SetColor("_Color", color);
        }



        yield return new WaitForSeconds(1.0f);
    }


}