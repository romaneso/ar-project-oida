﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceScript : MonoBehaviour
{
    public float thrust = 5.0f;
    public Rigidbody rb;
    public GameObject diceObject;

    public int diceCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(0, -thrust, 0, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        GetDiceCount();
    }

    void RollTheDice(Vector3 lastPos)
    {
        //diceObject.rigidbody.AddTorque(Vector3.Cross(lastPos, initPos) * 1000, orceMode.Impulse);
        lastPos.y += 12;
        //diceObject.rigidbody.AddForce(((lastPos - initPos).normalized) * (Vector3.Distance(lastPos, initPos)) * 25 * duceObject.rigidbody.mass);
    }

    void GetDiceCount()
    {
        if (Vector3.Dot(transform.forward, Vector3.up) > 1) diceCount = 5;
        if (Vector3.Dot(-transform.forward, Vector3.up) > 1) diceCount = 2;
        if (Vector3.Dot(transform.up, Vector3.up) > 1) diceCount = 3;
        if (Vector3.Dot(-transform.up, Vector3.up) > 1) diceCount = 4;
        if (Vector3.Dot(transform.right, Vector3.up) > 1) diceCount = 6;
        if (Vector3.Dot(-transform.right, Vector3.up) > 1) diceCount = 1;
        //Debug.Log("diceCount :" + diceCount);
    }
}
