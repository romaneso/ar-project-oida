﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewDiceScript : MonoBehaviour {

	static Rigidbody rb;
	public static Vector3 diceVelocity;

    private bool AddedVelocity = false;

	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		diceVelocity = rb.velocity;

        if (!AddedVelocity)
        {
            DiceNumberTextScript.diceNumber = 0;
            float dirX = Random.Range(0, 200);
            float dirY = -Random.Range(0, 200);
            float dirZ = Random.Range(0, 200);
            transform.rotation = Quaternion.identity;
            rb.AddForce(-transform.up * 500);
            rb.AddTorque(dirX, dirY, dirZ);

            //rb.AddRelativeForce(Random.onUnitSphere * 10);
            transform.eulerAngles = new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
            float speed = 200;
            rb.isKinematic = false;
            Vector3 force = transform.up;
            force = new Vector3(force.x, 1, force.z);
            rb.AddForce(force * speed);

            AddedVelocity = true;
        }
	}
}
