﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void DiceNumberChanged(int number);
public class DiceHitGroundCollider : MonoBehaviour {

    private static GameObject currentDice;
    private static GameObject nowDice;

    Vector3 diceVelocity;
    public static DiceNumberChanged listener;

    private static int diceValue;
    public static int DiceValue {
        get
        {
            return diceValue;
        }
        set
        {
            if(diceValue != value || currentDice != nowDice)
            {
                listener?.Invoke(value);
                diceValue = value;
            }
            
        }
    }

    private bool eventFired = false;

	// Update is called once per frame
	void FixedUpdate () {
		diceVelocity = NewDiceScript.diceVelocity;
	}

	void OnTriggerStay(Collider col)
	{
		if (diceVelocity.x == 0f && diceVelocity.y == 0f && diceVelocity.z == 0f)
		{
            nowDice = col.gameObject;

			switch (col.gameObject.name) {
			case "Side1":
				DiceNumberTextScript.diceNumber = 6;
                DiceValue = 6;
                break;
			case "Side2":
				DiceNumberTextScript.diceNumber = 5;
                DiceValue = 5;
                break;
			case "Side3":
				DiceNumberTextScript.diceNumber = 4;
                DiceValue = 4;
                break;
			case "Side4":
				DiceNumberTextScript.diceNumber = 3;
                DiceValue = 3;
                break;
			case "Side5":
				DiceNumberTextScript.diceNumber = 2;
                DiceValue = 2;
                break;
			case "Side6":
				DiceNumberTextScript.diceNumber = 1;
                DiceValue = 1;
                break;
			}

            currentDice = col.gameObject;



            Debug.Log("diceCount :" + DiceNumberTextScript.diceNumber);

        }
	}
}
