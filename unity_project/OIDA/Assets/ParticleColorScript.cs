﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleColorScript : MonoBehaviour
{
    public Gradient redGradient;
    public Gradient blueGradient;
    public Gradient greenGradient;
    public Gradient yellowGradient;

    private ParticleSystem ps;

    private int _color;
    public int ParticleColor
    {
        get
        {
            return _color;
        }
        set
        {
            if (_color == value) return;
            _color = value;

        }
    }
    void Start()
    {
        //init gradients
        redGradient = new Gradient();
        redGradient.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.red, 0.0f), new GradientColorKey(Color.white, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) });

        blueGradient = new Gradient();
        blueGradient.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.blue, 0.0f), new GradientColorKey(Color.white, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) });

        greenGradient = new Gradient();
        greenGradient.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.green, 0.0f), new GradientColorKey(Color.white, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) });

        yellowGradient = new Gradient();
        yellowGradient.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.yellow, 0.0f), new GradientColorKey(Color.white, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) });

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeToYellowGradient()
    {
        ps = GetComponent<ParticleSystem>();

        var col = ps.colorOverLifetime;
        col.enabled = true;

        Gradient gradient = yellowGradient;
        col.color = gradient;
    }

    public void ChangeToBlueGradient()
    {
        ps = GetComponent<ParticleSystem>();

        var col = ps.colorOverLifetime;
        col.enabled = true;

        Gradient gradient = blueGradient;
        col.color = gradient;
    }

    public void ChangeToGreenGradient()
    {
        ps = GetComponent<ParticleSystem>();

        var col = ps.colorOverLifetime;
        col.enabled = true;

        Gradient gradient = greenGradient;
        col.color = gradient;
    }

    public void ChangeToRedGradient()
    {
        ps = GetComponent<ParticleSystem>();

        var col = ps.colorOverLifetime;
        col.enabled = true;

        Gradient gradient = redGradient;
        col.color = gradient;
    }
}
